import { isEmptyExpression, parseTemplate } from '@angular/compiler';
import { isNull, THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Criteria, Target } from 'src/app/class/criteria';
import { Liste } from 'src/app/class/liste';
import { SearchService } from 'src/app/service/search.service';
import { ModaleService } from '../modale/modale.service';


/* manque d'erreurs en temps reel sur la modal du textArea , impossibilité de trigger un event sur change du textArea ,
impossibilité de select l'input ou tout element DOM de la modal pour y injecter des choses par exemple .. */

@Component({
  selector: 'app-quick-search',
  templateUrl: './quick-search.component.html',
  styleUrls: ['./quick-search.component.css']
})
export class QuickSearchComponent implements OnInit {

  searchControl: FormControl[];
  textControl: FormControl;
  selectControl: FormControl;
  criterias: Criteria[];
  list: Liste;
  statements: any[];
  url: string;
  valid: boolean;
  display_list: boolean;
  value_input: string[];
  focus_input: number;
  target_item: number;
  input_active: boolean[];
  user_try: boolean[];
  paste_str: string;
  paste_input_id: number;
  isError: boolean;

  constructor(private eRef: ElementRef, private modalService: ModaleService, private search: SearchService) {// eref represente ici le component lui meme
    this.search = new SearchService;
    this.criterias = [new Criteria("invoice_num", "string", [], true),
    new Criteria("payment_num", "string", [], true),
    new Criteria("RC_flag", "string", [new Target("", "=")], true)];
    this.list = new Liste;
    this.url = "";
    this.valid = false;
    this.display_list = true;/////////////////////////////////////////// TRUE POUR LE DEBUT
    this.value_input = ["", "", ""];
    this.focus_input = -1;
    this.target_item = -1;
    this.paste_input_id = -1;
    this.input_active = [false, false];
    this.searchControl = [new FormControl('', [Validators.required,
    Validators.pattern("^[0-9]+$"),
    Validators.maxLength(16),
    Validators.minLength(8)]),
    new FormControl('', [Validators.required,
    Validators.pattern("^[0-9]+$"),
    Validators.maxLength(16),
    Validators.minLength(8)]),
    new FormControl('')];
    this.textControl = new FormControl('');
    this.selectControl = new FormControl(';');
    this.user_try = [false, false];
    this.statements = [{ name: "red flag", value: "red_flag" },
    { name: "lead time", value: "lead_time" },
    { name: "in process", value: "in_process" },
    { name: "in statement", value: "in_statement" },];
    this.paste_str = "";
    this.isError = false;
  }

  ngOnInit(): void {
  }

  @HostListener('document:mousedown', ['$event'])
  clickout(event: any) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.focus_input = -1;
      this.user_try = [false, false];
    }
  }
  isUpdate() {
    if ((this.criterias[crit.statement].targets[0].value != "" && this.criterias[crit.statement].targets[0].value != null)
      || this.criterias[crit.payment].targets.length != 0 || this.criterias[crit.invoice].targets.length != 0) {
      this.valid = true;
    } else { this.valid = false; }
  }

  generateUrl() {
    let arr: Criteria[] = [];
    arr.push(this.criterias[crit.invoice], this.criterias[crit.payment]);
    this.criterias[crit.statement].targets[0].value != "" ? arr.push(this.criterias[crit.statement]) : null;
    this.list.setList(arr);
    this.url = this.search.search(this.list);
    alert(this.url);
  }

  addValue(input: number) {
    //console.log(this.searchControl[input].value);
    //console.log(this.searchControl[input].valid);
    if (this.searchControl[input].valid) {
      this.user_try[input] = false;
      this.criterias[input].targets.push(new Target(this.searchControl[input].value, "="));
      this.searchControl[input].setValue("");
      this.isUpdate();
    } else { this.user_try[input] = true; }
  }

  loadValue(input: number, item: number) {
    this.searchControl[input].setValue(this.criterias[input].targets[item].value);
    this.target_item = item;
    this.focus_input = input;
    this.user_try[input] = true;
  }
  updateValue(input: number) {//update d'une valeur existante d'une target de critere 

    if (this.searchControl[input].valid) {
      if (input == crit.statement) {
        this.criterias[crit.statement].targets[0].value = this.searchControl[input].value;
        this.isUpdate();
      } else {
        this.criterias[input].targets[this.target_item].value = this.searchControl[input].value;
        this.user_try[input] = false;
        this.searchControl[input].setValue("");
      }
      this.target_item = -1;
      this.focus_input = -1;
    } else {
      this.user_try[input] = true;
    }
  }
  deleteValue(input: number) {
    let item = this.target_item;
    if (input != crit.statement) {
      while (item + 1 < this.criterias[input].targets.length) {
        this.criterias[input].targets[item] = this.criterias[input].targets[item + 1];
        item++;
      }
      this.criterias[input].targets.pop();
    }
    this.searchControl[input].setValue("");
    this.target_item = -1;
    this.focus_input = -1;
    this.isUpdate();
    this.user_try[input] = false;
  }
  paste(active: boolean = false) {
    let delimiter = this.selectControl.value;
    let strTmp = this.textControl.value;
    if (delimiter != "\n") {// retire les retours à la ligne
      strTmp = strTmp.replace("\n", "");
    }
    if (delimiter != "\s") {// retire les espaces
      strTmp = strTmp.replace("\n", "");
    }
    let str: string[] = strTmp.split(delimiter);
    let strErrors: string[] = [];
    var i = str.length
    while (i--) {
      console.log(str[i].length);
      if ((str[i].search(/[^0-9$]/) != -1) || str[i].length < 8) {
        strErrors.push(str[i]);
        str.splice(i, 1);
      }else if( str[i] == ""){
        str.splice(i, 1);
      }else{
        if( active)
          this.criterias[this.paste_input_id].targets.push(new Target(str[i], "="));
      }
    }
    if (strErrors.length > 0) {
      this.isError = true;
      if( active){
        //this.eRef.nativeElement.querySelector("#rejected").append( strErrors.join(";"));
      }
    }
    this.isUpdate();
    if( active)
      this.paste_input_id = -1;

    active == true ? this.modalService.toggle("paste_modal"): null;
  }
  setFocus(inputID: string) {
    this.eRef.nativeElement.querySelector("#" + inputID).focus();
  }
  openModal(input: number) {
    this.modalService.toggle("paste_modal");
    this.paste_input_id = input;
  }
  clearArea() {
    this.textControl.setValue('');
  }
}

enum crit {
  invoice = 0,
  payment = 1,
  statement = 2,
}