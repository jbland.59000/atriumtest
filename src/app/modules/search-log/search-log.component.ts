import { Component, ElementRef, OnInit } from '@angular/core';
import { Search } from 'src/app/class/search';
import { User } from 'src/app/class/user';
import { AuthService } from 'src/app/service/auth.service';
import { SearchService } from 'src/app/service/search.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-search-log',
  templateUrl: './search-log.component.html',
  styleUrls: ['./search-log.component.scss']
})
export class SearchLogComponent implements OnInit {

  searches: Search[] = [];
  userTest: User = new User();
  searchAmount: number;
  focus: boolean[] = [false];
  expand: boolean = false;
  constructor(private searchService: SearchService, private authService: AuthService, private ref: ElementRef) {
    this.searchAmount = 3;
    for (let i = 0; i < 5; i++) {
      this.searches.push(this.searchService.findSearchTmp());
      console.log(this.searches[0].lastUseDate);
      this.userTest = authService.getUser();
    }
  }
  ngOnInit(): void {
    //this.ref.nativeElement.querySelector(".test").append(JSON.stringify(this.authService.getUser()) + "\n\n\n");
    //this.ref.nativeElement.querySelector(".test").append(JSON.stringify(this.searches[0]));
  }
}
