import { Component, ElementRef, HostListener, OnInit, Input, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ModaleService } from './modale.service';

@Component({
  selector: 'modal',
  templateUrl: './modale.component.html',
  styleUrls: ['./modale.component.scss']
})
export class ModaleComponent implements OnInit {
  @Input() id!: string;
  isActive: Boolean;

  constructor(private eRef: ElementRef, private modalService: ModaleService) {
    this.isActive = false;
  }
  ngOnInit(): void {
    document.body.appendChild(this.eRef.nativeElement);
    this.modalService.add(this);
  }
  ngOnDestroy(){
    this.modalService.remove(this.id);
  }

  @HostListener('document:mousedown', ['$event'])
  clickout(event: any) {
    if (event.target.className === 'modal' && this.isActive) {
      this.toggle();
    }
  }
  toggle(){
    this.isActive = !this.isActive;
  }
}
