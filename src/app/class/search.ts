import { Type } from "class-transformer";
import { Liste } from "./liste";
import { Access } from "./user";

export class Search {
    id?: number;
    name: string;
    lastUseDate: string;
    @Type(() => Liste)
    criterias: Liste;
    @Type(() => Liste)
    formats: Liste;
    @Type(() => Access)
    access: Access = new Access();

    constructor( criterias: Liste, formats: Liste, access: Access, lastUseDate?: string, name?: string) {
        this.criterias = criterias; 
        this.formats = formats;
        this.access = access;
        this.lastUseDate = lastUseDate ? lastUseDate : ""; 
        this.name = name ? name : "";
    }
}
