import { Type } from "class-transformer";
import { Criteria } from "./criteria";
import { Filtre, Format } from "./format";
import 'reflect-metadata';

export class Liste {
    id: number;
    name: string;
    link: string;
    @Type(() => Filtre, {
        discriminator: {
          property: '__type',
          subTypes: [
            { value: Criteria, name: 'Criteria' },
            { value: Format, name: 'Format' },
    ],},})
    list: Array<Filtre>;

    constructor(id?: number, name?: string, link?: string, list?: Array<Filtre>) {
        this.id = id ? id : -1
        this.name = name ? name : "new01";
        this.link = link ? link : "link01";
        this.list = list ? list : [];
    }
    setList( list: Array<Filtre>){
        this.list = list;
    }
    getList(): any[] {
        return this.list;
    }
}