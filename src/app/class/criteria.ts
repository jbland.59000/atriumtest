import { Filtre } from "./format";

export class Criteria extends Filtre {
    targets!: Target[];
    type: string;

    constructor(fieldname?: string, type?: string, targets?: Target[], is_active?: boolean) {
        super();
        if (fieldname)  { this.fieldname = fieldname;}else{ this.fieldname = "";}
        if (type)       { this.type = type;          }else{ this.type = "";}
        if (targets)    { this.targets = targets;    }else{ this.targets = [];}
        if (is_active)  { this.isActive = is_active;}else{ this.isActive = false;}
    }

    setCriteria(fieldname: string, type: string, targets: Target[], is_active: boolean){
        this.fieldname = fieldname;
        this.type = type; 
        this.targets = targets;
        this.isActive = is_active;
    }
}

export class Target {
    value: string;
    operator: string;

    constructor(value: string, operator: string) {
        this.value = value;
        this.operator = operator;
    }
}
