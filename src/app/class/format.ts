export abstract class Filtre {
    fieldname: string ="";
    isActive: boolean = false;
}

export class Format extends Filtre {
    surname: string;
    position: number;

    constructor(fieldname?: string, position?: number, surname?: string) {
        super();
        this.fieldname = fieldname ? fieldname : "";
        this.position = position ? position : -1;
        this.surname = surname ? surname : "";
        this.isActive = true;
    }
    setSurname(surname: string){
        this.surname = surname;
    }
    setPosition(pos: number){
        this.position = pos;
    }
    toggle(){
        this.isActive = !this.isActive;
    }

}