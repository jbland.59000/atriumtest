import { Type } from "class-transformer";
import { Comment } from "./comment";

export class Anomaly {
    id: number = -1;
    reasonCode: string;
    description: string;
    @Type(() => Comment)
    comments: Comment[] = [];
    status: string;
    constructor(description: string, reasonCode: string, stat: string){
        this.description = description ? description : "";
        this.reasonCode = reasonCode ? reasonCode : "";
        this.status = stat ? stat : "";
    }

}
