export class Comment {
    id: number;
    date: Date;
    content: string;
    commentFlag: string;
    author: number;

    constructor(author: number, content?: string, flag?: string){
        this.id = -1;
        this.date = new Date();
        this.content = content ? content : "";
        this.commentFlag = flag ? flag : "";
        this.author = author;
    }
}
