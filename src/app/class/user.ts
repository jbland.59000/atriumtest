import { Criteria } from "./criteria";
import { Format } from "./format";
import { Search } from "./search";
import { Type, plainToClass } from "class-transformer";

export class User {
    id?: number;
    name: string;
    password: string;
    saltKey: string;
    active: boolean;
    role: string;
    email: string;
    creationDate: Date = new Date();
    lastUpdDate: Date = new Date();
    lastUpdUser: number;
    creationUser: number;
    @Type(() => Access)
    access: Access = new Access();
    @Type(() => SmallComment)
    commentsRead: SmallComment[] = [];
    @Type(() => Search)
    searches: Search[] = [];
    @Type(() => Criteria)
    criterias: Criteria[] = [];
    @Type(() => Format)
    formats: Format[] = [];
    constructor(lastUpdUser: number = -1, name: string = "", password: string = "", salt: string = "", active: boolean = false,
                role: string = "", email: string = "") {
        this.creationUser = this.lastUpdUser = lastUpdUser;
        this.name = name;
        this.password = password;
        this.saltKey = salt;
        this.active = active;
        this.role = role;
        this.email = email;
    }
    test() {
        let data = {
            "id": 12,
            "cpt": 1
        };
        this.commentsRead.push(plainToClass(SmallComment, data));
    }
}

export class Access {
    corps: number[];
    subs: number[][];
    locs: number[][][];
    scacs: number[];
    scaclocs: number[][];
    constructor(corps?: number[], subs?: number[][], locs?: number[][][], scacs?: number[], scaclocs?: number[][]) {
        this.corps = corps ? corps : [];
        this.subs = subs ? subs : [];
        this.locs = locs ? locs : [];
        this.scacs = scacs ? scacs : [];
        this.scaclocs = scaclocs ? scaclocs : [];
    }
}

export class SmallComment {
    id: number;
    cpt: number;
    constructor(id: number, cpt: number) {
        this.id = id; this.cpt = cpt;
    }
}
