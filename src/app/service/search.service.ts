import { Injectable } from '@angular/core';
import { Liste } from '../class/liste';
import { Criteria } from '../class/criteria';
import { HttpParams } from "@angular/common/http";
import { getLocaleDateFormat } from '@angular/common';
import { Search } from '../class/search';
import { Format } from '../class/format';
import { plainToClass } from 'class-transformer';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  constructor() { }

  search(list_criteria: Liste, list_format?: Liste): string {
    let query = "?";
    list_criteria.list.forEach(item => { //https://stackoverflow.com/questions/6243051/how-to-pass-an-array-within-a-query-string
      if (item instanceof Criteria) {
        let field = item.fieldname;
        let tmpQuery = "";
        let typeFlag = item.type;
        item.targets.forEach(target => {
          tmpQuery += tmpQuery == "" && query == "?" ? field : "&" + field;
          switch (target.operator) {
            case ">": {
              tmpQuery += "gt " + target.value;
              break;
            }
            case "<": {
              tmpQuery += "lt " + target.value;
              break;
            }
            case "=": {
              tmpQuery += "=" + target.value;
              tmpQuery += typeFlag == "string" ? "[]" : "";
              break;
            }
          }
        });
        query += tmpQuery;
      }
    });
    return query;
  }
  findSearchFromUser(User: number, amount: number = 3): Search[] {
    return [];
  }

  findSearchTmp(): Search {
    return plainToClass(Search, {
      "id": 1,
      "name": "searchName1",
      "lastUseDate": "15/10/2021",
      "criterias": {
        "id": 1,
        "name":"listCritname1",
        "link":"linkC1",
        "list": [{
          "fieldname": "invoice number",
          "type": "number",
          "targets": [{
            "value": "01234567",
            "operator": "="
          },]
        }]
      },
      "formats": {
        "id": 2,
        "name":"listFormName1",
        "link":"linkF1",
        "list": [{
          "fieldname": "invoice number",
          "surname": "surnameF1",
          "isActive": true,
          "position": 1
        },{
          "fieldname": "payment number",
          "surname": "surnameF2", 
          "isActive": true,
          "position": 1
        }]
      },
      "access": {
        "corps": [180, 22],
        "subs": [[18050, 18060],[22500, 22600]],
        "locs": [[[51, 52, 53], [61, 62, 63]], [[501, 502], [601, 602]]],
        "scacs": [],
        "scaclocs": [[]]
      }
    });
  }
}
