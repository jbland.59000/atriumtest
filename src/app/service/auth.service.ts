import { Injectable } from '@angular/core';
import { plainToClass } from 'class-transformer';
import { User } from '../class/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
  getUser() {
    return plainToClass(User, {
      "id": 1,
      "name": "jean-baptiste landmann",
      "password": "pwd123",
      "active": true,
      "saltKey": "selClef",
      "role": "admin",
      "email": "abc@abc.com",
      "creationDate": "",
      "creationUser": "",
      "lastUpdDate": "",
      "lastUpdUser": "",
      "access": {
        "corps": [180, 22],
        "subs": [[18050, 18060],[22500, 22600]],
        "locs": [[[51, 52, 53], [61, 62, 63]], [[501, 502], [601, 602]]],
        "scacs": [],
        "scaclocs": [[]]
      },
      "commentsRead": [{ "id": 10, "cpt": 11},{ "id": 1, "cpt": 2}],
      "searches": [],
      "criterias": [],
      "formats": []
    });
  }
}
