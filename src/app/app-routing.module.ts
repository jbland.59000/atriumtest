import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModaleComponent } from './modules/modale/modale.component';
import { QuickSearchComponent } from './modules/quick-search/quick-search.component';
import { SearchLogComponent } from './modules/search-log/search-log.component';

const routes: Routes = [
  { path: 'search', component: QuickSearchComponent},
  { path: 'logs', component: SearchLogComponent },
  { path: '*', component: SearchLogComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
