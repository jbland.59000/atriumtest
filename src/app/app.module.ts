import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home/home.component';
import { QuickSearchComponent } from './modules/quick-search/quick-search.component';
import { ModaleComponent } from './modules/modale/modale.component';
import { SearchLogComponent } from './modules/search-log/search-log.component';
import { NotificationsComponent } from './modules/notifications/notifications.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    QuickSearchComponent,
    ModaleComponent,
    SearchLogComponent,
    NotificationsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
